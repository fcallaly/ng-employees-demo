export const environment = {
  production: true,
  emps_url: 'http://leaderboard.benivade.com:8081/employees',
  deps_url: 'http://leaderboard.benivade.com:8081/departments'
};
