import { Component, OnInit } from '@angular/core';
import { Employee } from '../model/employee';
import { EmployeesService } from '../services/employees.service';

@Component({
  selector: 'app-employees-table',
  templateUrl: './employees-table.component.html',
  styleUrls: ['./employees-table.component.css']
})
export class EmployeesTableComponent implements OnInit {

  employees: Employee[];

  constructor(private employeesService: EmployeesService) {}

  ngOnInit() {
    this.employeesService.findAll().subscribe(
      employeesArray => {
        console.log(employeesArray);
        this.employees = employeesArray;
      },
      error => {
        console.log(error);
      }
    );
  }
}
