import { Employee } from './employee';
import { Department } from './department';

describe('Employee', () => {
  it('should create an instance', () => {
    expect(new Employee(1, 'John', 2000.99, new Department(1, 'Dev'))).toBeTruthy();
  });
});
