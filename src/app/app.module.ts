import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { EmployeesTableComponent } from './employees-table/employees-table.component';
import { DepartmentsTableComponent } from './departments-table/departments-table.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeesTableComponent,
    DepartmentsTableComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
