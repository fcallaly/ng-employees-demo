import { Component, OnInit } from '@angular/core';
import { DepartmentsService } from '../services/departments.service';
import { Department } from '../model/department';

@Component({
  selector: 'app-departments-table',
  templateUrl: './departments-table.component.html',
  styleUrls: ['./departments-table.component.css']
})
export class DepartmentsTableComponent implements OnInit {

  departments: Department[];

  constructor(private departmentsService: DepartmentsService) {}

  ngOnInit() {
    this.departmentsService.findAll().subscribe(
      departmentsArray => {
        console.log(departmentsArray);
        this.departments = departmentsArray;
      },
      error => {
        console.log(error);
      }
    );
  }
}
