# Angular Lab
## Outline
The objectives of this lab are

•	To become familiar with creating a new angular project. 

•	To become familiar with binding between html and typescript code in angular.

•	To become familiar with making HTTP requests with the angular HttpClient.

## Instructions
In this lab we will retrieve a list of employees from a Java Spring Boot Employee REST API. These instructions assume that this backend REST service is running at: http://leaderboard.benivade.com:8081/employees

Change the backend URL to where ever your service is running. You should be able to point a browser at the backend URL and use it's swagger UI to get familiar with the REST API.

-----------------------------

### Step 1
In a sensible directory, create a new angular project. The project should be called angular-employees-exercise:

```bash
ng new angular-employees-exercise
```

-----------------------------

### Step 2
Delete the contents of the app.component.html file and replace with a heading and a paragraph containing the title variable from your app.component.ts class, e.g.
```html
<h1>Employees List</h1>
<p>{{title}} component is running</p>
```

-----------------------------

### Step 3
Create a new model class called Employee that has the following attributes:

•	id: number

•	name: string

•	salary: number

e.g.
```bash 
cd src/app
mkdir model
cd model
ng generate class employee
```

-----------------------------

### Step 4
Create a service class to read from the web API
```bash
cd src/app
ng generate service services/employees
```

-----------------------------

### Step 5
Include the HttpClientModule in the imports in app.module.ts e.g.
```typescript
import { HttpClientModule } from '@angular/common/http';

imports: [
    BrowserModule,
    HttpClientModule
  ]
```

-----------------------------

### Step 6
Include a HttpClient in the constructor of the service class e.g.
```typescript
constructor(private httpClient: HttpClient) { }
```
Now implement a function in your service class that returns an Observable<Employee> and does a http get to: http://leaderboard.benivade.com:8081/employees


e.g (the below is for Trade objects, you need to change it to Employee objects!!):
```typescript
public findAll(): Observable<Trade[]> {
    return this.http.get<Trade[]>(‘http://leaderboard.benivade.com:8081/employees’);
  }
```

-----------------------------

### Step 7
Include your service class in the constructor of the class in app.component.ts

In the ngOnInit function call the service method to getEmployees

-----------------------------

### Step 8
Display a table of the returned employees in app.component.html

-----------------------------

### Optional Extras
These additions can be made if you get the basic application above complete

•	Add list of departments to your page, retrieved from the departments endpoint i.e. http://leaderboard.benivade.com:8081/departments
