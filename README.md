# AngularEmployeesExercise

This project is a training exercise for creating a simple angular CRUD UI.

This frontend interfaces to the backend in: https://bitbucket.org/fcallaly/spring-web-employees

The dev/post branch of ng-employees-demo includes a simple HTTP POST to create a department record.
The dev/post branch of this repository should use the dev/dep-foreign-key branch of spring-web-employees.

The default backend is currently set to http://leaderboard.benivade.com:8081
Point a web browser at the backed URL to check if the backend is running.
The backend has a swagger-ui to directly explore the API.

To run the frontend, you should have node, npm and angular 8 installed (it may work with newer versions of angular):
* clone the repo and switch to the desired branch:
* `npm install` to download dependencies
* `ng serve` to run the dev server
* Point your browser at http://localhost:4200

There is an example document for a student exercise to create this project in labs/

See the angular documentation below.

-----------------------------------------------------------------------------------------------------------------------

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
